const config = sails.config.custom;

module.exports = {
    findAll: async (req, res) => {

        const url = `${config.fhirServer}/Slot?identifier=useFor|TCU-H707`;

        try {
            const response = await script._fetch(url);

            res.status(200).json({ success: true, message: 'Success Found!', response: response });
        } catch (err) {
            res.status(err.response.status).json({ success: false, message: err.message });
        }
    },
    fincOne: async (req, res) => {

        const courseId = req.params.courseId;
        const url = `${config.fhirServer}/Slot/${courseId}`;

        try {
            const response = await script._fetch(url);

            res.status(200).json({ success: true, message: 'Success Found!', response: response });
        } catch (err) {
            res.status(err.response.status).json({ success: false, message: err.message });
        }
    },
    findByUser: async (req, res) => {

        const userId = req.params.userId;
        const role = req.params.role;
        let url = '';

        if (role == undefined) {
            res.status(400).json({ success: false, message: 'No Role!' });
        } else {
            switch (role) {
                case 'student':
                    url = `${config.fhirServer}/Slot?_has:Appointment:slot:patient=${userId}&identifier=useFor|TCU-H707`;
                    break;
                case 'teacher':
                    url = `${config.fhirServer}/Slot?_has:Appointment:slot:practitioner=${userId}&identifier=useFor|TCU-H707`;
                    break;
                case 'admin':
                    url = `${config.fhirServer}/Slot?identifier=useFor|TCU-H707`;
                    break;
            }

            try {
                const response = await script._fetch(url);

                res.status(200).json({ success: true, message: 'Success Found!', response: response });
            } catch (err) {
                res.status(err.response.status).json({ success: false, message: err.message });
            }
        }
    },
    create: async (req, res) => {

        const coursename = req.body.coursename;
        const url = `${config.fhirServer}/Slot`;

        if (coursename == undefined)
            res.status(400).json({ success: false, message: 'No CourseName!' });
        else {
            try {
                let slot = fhirResource.slot;

                slot['comment'] = coursename;

                const option = {
                    body: JSON.stringify(slot),
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json'
                    }
                }
                const response = await script._fetch(url, option);

                res.status(200).json({ success: true, message: 'Success Created!', response: response });
            } catch (err) {
                res.status(err.response.status).json({ success: false, message: err.message });
            }
        }
    },
    delete: async (req, res) => {

        const courseId = req.params.courseId;
        const url = `${config.fhirServer}/Slot/${courseId}`;

        try {
            const option = {
                method: 'DELETE'
            }
            const response = await script._fetch(url, option);

            res.status(200).json({ success: true, message: 'Success Deleted!' });
        } catch (err) {
            res.status(err.response.status).json({ success: false, message: err.message });
        }
    },
    update: async (req, res) => {

        const courseId = req.params.courseId;
        const url = `${config.fhirServer}/Slot/${courseId}`;
        let rule = [];
        const coursename = req.body.coursename;

        if (coursename != undefined)
            rule.push({ op: 'replace', path: '/comment', value: coursename });

        try {
            const option = {
                method: 'PATCH',
                body: JSON.stringify(rule),
                headers: {
                    'content-type': 'application/json-patch+json'
                }
            }
            const response = await script._fetch(url, option);

            res.status(200).json({ success: true, message: 'Success Updated!', response: response });
        } catch (err) {
            res.status(err.response.status).json({ success: false, message: err.message });
        }
    }
}